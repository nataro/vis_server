from django.contrib.auth import get_user_model
from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import View

from rest_framework.views import APIView
from rest_framework.response import Response

from environment.models import Brand, SensorData

import datetime
import time


# User = get_user_model()

class HomeView(View):
    '''
    templates/charts.htmlを返す。実際のグラフを描画するメイン処理
    '''

    def get(self, request, *args, **kwargs):
        return render(request, 'charts.html')


class ChartData(APIView):
    '''
    rest_frameworkのAPIWiewを用いて処理している
    dataで示す内容がrest_frameworkを介して渡される
    '''
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        # 時間軸で並べて可視化することを踏まえて、時間（カラム名'time'）でソートしておく
        # orderd_by_time = SensorData.objects.all().filter(time__range=['2017-12-14','2017-12-15'])
        orderd_by_time = SensorData.objects.all().filter(time__year='2017',
                                                         time__month='12',
                                                         time__day='12').order_by('time')
        # 各カラム毎にデータを受け取る
        # このとき flat=True にしないとタプルになったデータが返ってくる
        temps = orderd_by_time.values_list('temp', flat=True)
        humids = orderd_by_time.values_list('humid', flat=True)
        atmos = orderd_by_time.values_list('atmo', flat=True)
        times = orderd_by_time.values_list('time', flat=True)
        '''
        highchartのdatetimeはmilisedondsで渡すと素直に表示されるみたい。
        SendorData.cbjectsから受け取った直後は、datetime.datetime()型なので、milisecondsの値となるように以下の処理を行う
        さらに、REST-frameWorkでdatatime型を返すとき、自動的にタイムゾーン分の時間がオフセットされてかえされるみたい。
        この問題の対策については、HighChartに渡すデータにのみ、'Asia/Tokyo'の時間分(9時間)を加算して対応することにする。
        なぜか、9時間ではなくて18時間加算すると上手くいく
        '''
        tz_ofsset = 18 * 3600 * 1000
        times = [time.mktime(x.timetuple()) * 1000 + tz_ofsset for x in times]

        # Highchart に渡すデータを[[time1,temp1],[time2,temp2],・・・]という形にしたいので内包表記でまとめる
        time_temp = [[x, y] for x, y in zip(times, temps)]
        time_humids = [[x, y] for x, y in zip(times, humids)]
        time_atmos = [[x, y] for x, y in zip(times, atmos)]

        data = {
            # 第１要素のtimeでソートしてからHighchartに渡すようにする
            "temps": time_temp,
            "humids": time_humids,
            "atmos": time_atmos,
        }

        return Response(data)


class ChartDataDate(APIView):
    '''
    rest_frameworkのAPIWiewを用いて処理している
    dataで示す内容がrest_frameworkを介して渡される
    '''
    authentication_classes = []
    permission_classes = []

    def get(self, request, u_year, u_month, u_day, format=None):
        # specified_by_model = SensorData.objects.all().filter(model=sensor_model)
        orderd_by_time = SensorData.objects.all().filter(time__year=u_year,
                                                         time__month=u_month,
                                                         time__day=u_day).order_by('time')

        # "model"カラムをGroupByして、"model"の各要素が何個づつ存在するか確認
        from django.db.models.aggregates import Count
        count_models = SensorData.objects.all().values("model").annotate(cnt=Count("*"), )
        # 結果はdjango.db.models.query.QuerySet型、[{'model':'no1','cnt':20},{'model':'no2','cnt':20},...]
        # print(count_models)
        # pythonのリストで、'model'のリストだけ抜き取る（flat=True にしないとタプルになる）
        models = count_models.values_list('model', flat=True)
        # print(models)

        sensor_data = {}

        for model_name in models:
            filter_by_model = SensorData.objects.all().filter(model=model_name)
            orderd_by_time = filter_by_model.filter(time__year=u_year,
                                                    time__month=u_month,
                                                    time__day=u_day).order_by('time')

            # 各カラム毎にデータを受け取る
            # このとき flat=True にしないとタプルになったデータが返ってくる
            temps = orderd_by_time.values_list('temp', flat=True)
            humids = orderd_by_time.values_list('humid', flat=True)
            atmos = orderd_by_time.values_list('atmo', flat=True)
            times = orderd_by_time.values_list('time', flat=True)

            '''
            highchartのdatetimeはmilisedondsで渡すと素直に表示されるみたい。
            SendorData.cbjectsから受け取った直後は、datetime.datetime()型なので、milisecondsの値となるように以下の処理を行う
            さらに、REST-frameWorkでdatatime型を返すとき、自動的にタイムゾーン分の時間がオフセットされてかえされるみたい。
            この問題の対策については、HighChartに渡すデータにのみ、'Asia/Tokyo'の時間分(9時間)を加算して対応することにする。
            なぜか、9時間ではなくて18時間加算すると上手くいく
            '''

            tz_offset = 18 * 3600 * 1000
            times = [time.mktime(x.timetuple()) * 1000 + tz_offset for x in times]

            # Highchart に渡すデータを[[time1,temp1],[time2,temp2],・・・]という形にしたいので内包表記でまとめる
            time_temp = [[x, y] for x, y in zip(times, temps)]
            time_humids = [[x, y] for x, y in zip(times, humids)]
            time_atmos = [[x, y] for x, y in zip(times, atmos)]

            data = {
                # 第１要素のtimeでソートしてからHighchartに渡すようにする
                "temps": time_temp,
                "humids": time_humids,
                "atmos": time_atmos,
            }

            sensor_data[model_name] = data

        return Response(sensor_data)


class ChartDataDateModel(APIView):
    '''
    rest_frameworkのAPIWiewを用いて処理している
    dataで示す内容がrest_frameworkを介して渡される
    '''
    authentication_classes = []
    permission_classes = []

    def get(self, request, u_year, u_month, u_day, u_model, format=None):
        filter_by_model = SensorData.objects.all().filter(model=u_model)
        orderd_by_time = filter_by_model.filter(time__year=u_year,
                                                time__month=u_month,
                                                time__day=u_day).order_by('time')

        # 各カラム毎にデータを受け取る
        # このとき flat=True にしないとタプルになったデータが返ってくる
        temps = orderd_by_time.values_list('temp', flat=True)
        humids = orderd_by_time.values_list('humid', flat=True)
        atmos = orderd_by_time.values_list('atmo', flat=True)
        times = orderd_by_time.values_list('time', flat=True)
        '''
        highchartのdatetimeはmilisedondsで渡すと素直に表示されるみたい。
        SendorData.cbjectsから受け取った直後は、datetime.datetime()型なので、milisecondsの値となるように以下の処理を行う
        さらに、REST-frameWorkでdatatime型を返すとき、自動的にタイムゾーン分の時間がオフセットされてかえされるみたい。
        この問題の対策については、HighChartに渡すデータにのみ、'Asia/Tokyo'の時間分(9時間)を加算して対応することにする。
        なぜか、9時間ではなくて18時間加算すると上手くいく
        '''
        tz_ofsset = 18 * 3600 * 1000
        times = [time.mktime(x.timetuple()) * 1000 + tz_ofsset for x in times]

        # Highchart に渡すデータを[[time1,temp1],[time2,temp2],・・・]という形にしたいので内包表記でまとめる
        time_temp = [[x, y] for x, y in zip(times, temps)]
        time_humids = [[x, y] for x, y in zip(times, humids)]
        time_atmos = [[x, y] for x, y in zip(times, atmos)]

        data = {
            # 第１要素のtimeでソートしてからHighchartに渡すようにする
            "temps": time_temp,
            "humids": time_humids,
            "atmos": time_atmos,
        }

        return Response(data)


# 各HTMLへの接続を設定している

from django.views.generic import TemplateView


class SensorDataListView(TemplateView):
    template_name = "sensor_data_table.html"

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)

        sensor_datas = SensorData.objects.all()
        context['sensor_data'] = sensor_datas

        return render(self.request, self.template_name, context)


class SensorDataChartView(TemplateView):
    template_name = "sensor_data_charts.html"

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)

        # sensor_datas = SensorData.objects.all()
        # context['sensor_data'] = sensor_datas

        return render(self.request, self.template_name, context)


class SensorDataChartViewProto(TemplateView):
    template_name = "sensor_data_charts_2.html"

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)

        # sensor_datas = SensorData.objects.all()
        # context['sensor_data'] = sensor_datas

        return render(self.request, self.template_name, context)
