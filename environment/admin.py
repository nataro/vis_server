from django.contrib import admin

from environment.models import Brand, SensorData

from environment.actions import export_as_csv_action


class SensorDataAdmin(admin.ModelAdmin):
    list_display = ['model', 'time', 'temp', 'humid', 'atmo']
    # 管理画面にCSVデータをエクスポートするactionを追加する
    actions = [export_as_csv_action("CSV Export", fields=['model', 'time', 'temp', 'humid', 'atmo'])]


admin.site.register(Brand)
admin.site.register(SensorData, SensorDataAdmin)


