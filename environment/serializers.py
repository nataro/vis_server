from rest_framework import serializers

from environment.models import Brand, SensorData


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ('code_name', 'full_name', 'remarks',)


class SensorDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = SensorData
        fields = ('model', 'time', 'temp', 'humid', 'atmo', 'body', 'brand',)
