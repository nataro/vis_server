from django.db import models


class Brand(models.Model):
    """
     【Brand】お酒の銘柄に関するモデル
    code_name:コードネーム（POSTなどで使用するために英数字を想定）
    full_name：フルネーム（表示などを想定して日本語など）
    remarks：備考欄
    """
    code_name = models.CharField(max_length=25, unique=True)
    full_name = models.CharField(max_length=255)
    remarks = models.TextField(blank=True)

    def __str__(self):
        return self.full_name


class SensorData(models.Model):
    """
    【SensorData】センサーから取得した環境データに関するモデル
    model：機種名
    time：日時
    temp：気温
    humid：湿度
    atmo：気圧
    body：メモ代わり
    brand：酒の銘柄
    """
    model = models.CharField(max_length=255, blank=True)
    time = models.DateTimeField()
    temp = models.FloatField(blank=True, null=True)
    humid = models.FloatField(blank=True, null=True)
    atmo = models.FloatField(blank=True, null=True)
    body = models.CharField(max_length=255, blank=True)
    brand = models.ForeignKey(Brand, blank=True, null=True)

    def __str__(self):
        return self.model
