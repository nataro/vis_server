import unicodecsv
from django.http import HttpResponse

'''
Djangoの管理画面で使用するactionを自前で定義するときに利用する関数を定義している

CSVのエクスポートは下記参照
https://gist.github.com/mgerring/3645889

admin.pyの中で、対象となるモデルクラスの
    actions = [export_as_csv_action("CSV Export", fields=['model', 'time', 'temp', 'humid', 'atmo'])]
に追加すると使えるようになるはず

fieldにForeignKeyのものを指定すると、オブジェクトそのものが指定されるので、そのままでは有効なデータを記録出来ないみたい。

'''


def export_as_csv_action(description="Export selected objects as CSV file",
                         fields=None, exclude=None, header=True):
    """
    This function returns an export csv action
    'fields' and 'exclude' work like in django ModelForm
    'header' is whether or not to output the column names as the first row
    """

    def export_as_csv(modeladmin, request, queryset):
        opts = modeladmin.model._meta

        if not fields:
            field_names = [field.name for field in opts.fields]
        else:
            field_names = fields

        # response = HttpResponse(mimetype='text/csv')
        # response['Content-Disposition'] = 'attachment; filename=%s.csv' % unicode(opts).replace('.', '_')
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s.csv' % str(opts).replace('.', '_')

        writer = unicodecsv.writer(response, encoding='utf-8')
        if header:
            writer.writerow(field_names)
        for obj in queryset:
            row = [getattr(obj, field)() if callable(getattr(obj, field)) else getattr(obj, field) for field in
                   field_names]
            writer.writerow(row)
        return response

    export_as_csv.short_description = description
    return export_as_csv
