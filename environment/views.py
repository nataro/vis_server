from django.shortcuts import render

from environment.models import Brand, SensorData
from environment.serializers import BrandSerializer, SensorDataSerializer

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class BrandDataList(APIView):
    """
    BrandのコードネームでソートしたデータをGET、POST
    """

    def get(self, request, code_name, format=None):
        brand = Brand.objects.get(code_name=code_name)
        sensor_datas = SensorData.objects.filter(brand=brand)
        serializer = SensorDataSerializer(sensor_datas, many=True)
        return Response(serializer.data)

    def post(self, request, code_name, format=None):
        brand = Brand.objects.get(code_name=code_name)
        # brand はmodels.pyに定義したBland型のインスタンスなので、そのままポストしても受け付けられない
        # 一旦Bland型インスタンスのid（brand.id）をrequestのdataの中に「'brand'」として格納しておいて、
        # そのrequest.dataをシリアライザに掛けるようにする
        request.data['brand'] = brand.id
        serializer = SensorDataSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



