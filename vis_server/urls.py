"""vis_server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from django.contrib import admin
from environment import views

from environment.views import BrandDataList
from charts.views import HomeView, ChartData, SensorDataListView, SensorDataChartView, ChartDataDate, \
    ChartDataDateModel, SensorDataChartViewProto

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # urlから得た「code_name」は views.BrandDataList()で受け取って処理する
    url(r'^api/brand_detail/(?P<code_name>[a-zA-Z0-9-]+)/data_list/$', BrandDataList.as_view(), name='brand_data_list'),
    url(r'^$', HomeView.as_view(), name='home'),
    # HighChart用のデータをRESTapi経由で取得するview
    url(r'^api/chart/data/$', ChartData.as_view()),
    # 「****/**/**」という日付限定の正規表現を指定
    url(r'^api/chart/data/(?P<u_year>\d{2,4})/(?P<u_month>\d{1,2})/(?P<u_day>\d{1,2})/$', ChartDataDate.as_view()),
    # 「****/**/**」という日付限定の正規表現を指定。さらに'model'名でフィルタ
    url(r'^api/chart/data/(?P<u_year>\d{2,4})/(?P<u_month>\d{1,2})/(?P<u_day>\d{1,2})/(?P<u_model>[a-zA-Z0-9-_]+)/$',
        ChartDataDateModel.as_view()),
    # bootstrap list test
    url(r'^sensor_data_list/', SensorDataListView.as_view()),
    url(r'^sensor_data_chart/', SensorDataChartView.as_view()),
    url(r'^sensor_data_chart_proto/', SensorDataChartViewProto.as_view()),
]
